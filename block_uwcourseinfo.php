<?php
/******************************************************************************
* UW Madison Course Information Block
*
* Display roster to Moodle associations and course information.
*
* Author: Nick Koeppen
******************************************************************************/
require_once($CFG->dirroot."/blocks/html/block_html.php");

class block_uwcourseinfo extends block_html {

    /**
     * Tell Moodle that we have global settings
     *
     * @return bool
     */
    public function has_config(){
        return true;
    }

    function init() {
		$this->title = get_string('blocktitle', 'block_uwcourseinfo');
	}

	function instance_create() {
	    $this->reset_content();
		return true;
	}

	function instance_config_save($data, $nolongerused = false){
	    parent::instance_config_save($data);
	}

	function specialization() {
	    global $COURSE, $PAGE;

		$this->title = isset($this->config->title) ? format_string($this->config->title) : $this->title;
		/* Remove current configuration and reset to default */
		if (optional_param('uwcourseblockreset', false, PARAM_BOOL) && confirm_sesskey()) {
		    // Very hacky workaround when debugging causes messages to start printing header before redirect
		    if ($PAGE->pagelayout == 'redirect') {
		        return;
		    }
		    $this->reset_content();	// Reset content

		    $url = $PAGE->url;
		    $url->remove_params('uwcourseblockreset');
		    redirect($url);
		}
	}

	function applicable_formats() {
	    // Default case: the block can be used in all course types
	    return array('site' => false, 'my' => false, 'course-view' => true);
	}

	function get_content() {
	    global $COURSE, $PAGE, $OUTPUT, $USER;

		if ($this->content !== NULL && $COURSE->id == SITEID) {
			return $this->content;	//Only on course pages
		}

		/* Create HTML formatted display */
		parent::get_content();	//Get HTML content

		$html = '';
		if (!empty($USER->editing)) {
		    $sesskey = sesskey();
		    // Reset link
		    $link = $PAGE->url->out(true, array('sesskey'=>$sesskey, 'uwcourseblockreset'=>1));
		    $label = get_string('rebuild', 'block_uwcourseinfo');
		    $icon = $OUTPUT->pix_icon('t/reload', $label);
		    $html .= "<div style='float:left;'><a href='$link'>$icon $label</a></div>";
		    // Edit link
		    $params = array('id'=>$COURSE->id, 'sesskey'=>$sesskey, 'bui_editid'=>$this->instance->id);
		    $link = new moodle_url('/course/view.php', $params);
		    $label = get_string('edit');
		    $icon = $OUTPUT->pix_icon('t/edit', $label);
		    $html .= "<div style='float:right;'><a href='$link'>$icon $label</a></div>";
		    $html .= "<div class='divider'><hr></div>";
		}

	    $this->content->text = $html.$this->content->text;

// 	    // Live reset each time - designer mode
// 	    $siteconfig = get_config("block_uwcourseinfo");
//     	$config = $siteconfig;
//     	$config->roles = explode(',', $config->roles);
// 	    $this->content->text = $this->reset_content($COURSE->id, $config);

		return $this->content;
	}

	function reset_content($courseid = null){
		global $DB, $OUTPUT, $COURSE;

		if (!isset($courseid)) {
		    $courseid = $COURSE->id;
		}

		if (! ($context = context_course::instance($courseid))) {
			return false;	//No moodle course with given id
		}

		/* Determine configuration settings */
		$siteconfig = get_config("block_uwcourseinfo");
		if(!$siteconfig->allowinstanceconfig || !isset($this->config)){
			$config = $siteconfig;
			$config->roles = explode(',', $config->roles);
		}else{
			$config = $this->config;
			if(isset($this->config->roles)){
				$config->roles = array_keys($config->roles);
			}else{
				$config->roles = array();
			}
		}

		/* Get course roster information */
		//Sections
		$orderBy = 'term, catalog_number, section_number, subject_code';
		$dbSections = $DB->get_records("enrol_wisc_coursemap", array('courseid' => $courseid), $orderBy);
		if (empty($dbSections)) {
			return false;	//No mapped sections
		}
		//Groups and users by group
		if (! ($uroles  = $config->roles)) {
		    return false;	//No role users to display
		}
		$aliasnames = $DB->get_records_menu('role_names', array('contextid'=>$context->id),'','roleid, name');
		$ufields = 'u.id, r.id as roleid, r.name as role, u.firstname, u.lastname';
		$usort   = 'r.sortorder ASC, u.lastname ASC';
		$users = array();
		$groupids = array_keys(groups_get_all_groups($courseid));
		foreach($groupids as $groupid){
		    $users[$groupid] = get_role_users($uroles, $context, false, $ufields, $usort, null, $groupid);
		}

		/* Reorganize courses and establish group map */
		$courses = array();
		foreach($dbSections as $dbSect) {
		    //New coursemap fields need defaults
		    $isisid   = !empty($dbSect->isis_course_id) ? $dbSect->isis_course_id : $dbSect->class_number;
		    $secttype = !empty($dbSect->type) ? $dbSect->type : 'SEC';
		    $subj 	  = !empty($dbSect->subject) ? $dbSect->subject : $dbSect->subject_code;

		    /* Group sections by term and ISIS courseid */
		    if (!isset($courses[$dbSect->term][$isisid])) {
		        $courses[$dbSect->term][$isisid] = array();
		        $courses[$dbSect->term][$isisid]['users'] = array();
		    }
		    $course = &$courses[$dbSect->term][$isisid];
		    $course['catalognumber'] = $dbSect->catalog_number;
		    //Collect subjects
		    $course['subjects'][$dbSect->subject_code] = preg_replace('/ /','', $subj);

		    /* Assign users to UW courses */
		    if (!empty($users[$dbSect->groupid])) {
		        foreach($users[$dbSect->groupid] as $user){
		            /* Add user to course */
	        		if(!isset($course['users'][$user->id])){
	        		    $course['users'][$user->id] = $user;
			            $course['users'][$user->id]->sections = array();
			        }
			        $course['users'][$user->id]->sections[$secttype][$dbSect->section_number][] = $dbSect->subject_code;
			    }
		    }else{
		        //Unattributed sections
		        $course['sections'][$secttype][$dbSect->section_number][] = $dbSect->subject_code;
		    }
		}

		$nameformat = array('firstname' => $config->firstnames, 'nameorder' => $config->nameorder);

		/* Compile roster information into HTML */
		$divider = "<div class='divider'><hr></div>";
		$termhtml = array();
		foreach($courses as $term => $uwcourses){
		    $thtml = '';
		    $coursehtml = array();
		    foreach($uwcourses as $uwcourse){
			    $chtml = $this->print_course($term, $uwcourse['subjects'], $uwcourse['catalognumber']);
			    $chtml .= $this->print_users($uwcourse, $nameformat);
			    $coursehtml[] = $chtml;
		    }
		    $thtml .= implode($divider, $coursehtml);	//Divide courses
		    $termhtml[] = $thtml;
		}

		$html = implode($divider, $termhtml)."\n$divider\n";

		$data = new stdClass();
		$data->title = $config->title;
		$data->text['text']   = $html;
		$data->text['format'] = FORMAT_HTML;    //Only HTML format
		$data->roles = array_fill_keys($config->roles, 1);
		$data->firstnames = $config->firstnames;
		$data->nameorder = $config->nameorder;
		$this->instance_config_save($data);

		return $html;
	}

	function print_course($term, $subjects, $catalognumber){
	    global $OUTPUT;

		sort($subjects);

		//Solution to wrap that only works for the next 900+ years ;)
		$namewospace = str_replace(" ", 'X', $this->termname($term));
		$coursename = join(' / ',$subjects)." $catalognumber $namewospace";
		$coursename = str_replace('X2', ' 2', wordwrap($coursename, 22, '<br>'));

		return $OUTPUT->heading($coursename, 4, 'course');
	}

	function print_users(array $uwcourse, $nameformat){
	    $html = '';
	    foreach($uwcourse['users'] as $user){
	        $html .= $this->print_user_name($user, $nameformat);
	        $html .= $this->print_sections($user->sections, $uwcourse['subjects']);
	    }

	    if(!empty($uwcourse['sections'])){
	        $html .= $this->print_sections($uwcourse['sections'], $uwcourse['subjects']);
	    }

	    return $html;
	}

	function print_users_list(array $uwcourse, $nameformat){
	    $html .= "<ul class='users'>\n";
	    foreach($uwcourse['users'] as $user){
	        $html .= "<li class='user'>\n";
	        $html .= $this->print_user_name($user, $nameformat);
	        $html .= $this->print_sections_list($user->sections, $uwcourse['subjects']);
	        $html .= "</li>\n";
	    }
	    $html .= "</ul>\n";

	    if(!empty($uwcourse['sections'])){
	        $html .= $this->print_sections_list($uwcourse['sections'], $uwcourse['subjects']);
	    }

	    return $html;
	}

	function print_user_name($user, $format){
		global $COURSE;

		$url = new moodle_url('/user/view.php', array('id' => $user->id, 'course' => $COURSE->id));
		if ($format['firstname'] == 'no') {
			$firstname = $user->firstname[0].'.';
		} else {
			$firstname = $user->firstname;
		}
		if ($format['nameorder'] == 'normal') {
			$dispname = mb_convert_case("$firstname $user->lastname", MB_CASE_TITLE);
		} else {
			$dispname = mb_convert_case("$user->lastname, $firstname", MB_CASE_TITLE);
		}

		return "<div class='username'><a href='$url'>$dispname</a></div>";
	}

	function print_sections($sections, $subjects){
	    $subjectCodes = array_keys($subjects);

	    $content = "";
	    foreach($sections as $type => $numbers){
	        $sectionNum = count($numbers);
	        $content .= "&nbsp;&nbsp;&nbsp;<b>$type</b> ";
	        $sectnumbers = array();
	        foreach($numbers as $number => $sectsubjects){
	            $sectnumber = $number;
	            $onlysubjects = array_diff($subjectCodes, $sectsubjects);
	            if(!empty($onlysubjects)){
	                $onlysubjects = implode(', ', $onlysubjects);
	                $sectnumber .= " (only $onlysubjects)";
	            }
	            $sectnumbers[] = $sectnumber;
	        }
	        $content .= implode(', ', $sectnumbers).'<br>';
	    }

	    return html_writer::tag('p', $content);
	}

	function print_sections_list($sections, $subjects){
		$subjectCodes = array_keys($subjects);

		$html = "<ul class='coursesections'>\n";
		foreach($sections as $type => $numbers){
			$sectionNum = count($numbers);
			$html .= "<li class='sectiontype'>\n";
			$html .= "<span class='type'>$type</span>\n";
			$html .= "<ul class='sections'>\n";
			foreach($numbers as $number => $sectsubjects){
				$html .= "<li class='section'>";
				$html .= $number;
				$onlysubjects = array_diff($subjectCodes, $sectsubjects);
				if(!empty($onlysubjects)){
					$html .= "<ul class='onlysubjs'>";
					foreach($onlysubjects as $onlysubject){
						$html .= "<li>$onlysubject</li>";
					}
					$html .= "</ul>";
				}
				$html .= "</li>\n";
			}
			$html .= "</ul>\n</li>";
		}
		$html .= "</ul>\n";

		return $html;
	}

    function termname($term){
		$term = (string)$term;
		if (strlen($term) != 4) {
			return "Invalid term";
		}

		$century = (int)$term[0];
		$decades = (int)$term[1];
		$decyear = (int)$term[2];
		$yearsem = (int)$term[3];
		if ($century == 0) {
			$year = 1900 + $decades*10 + $decyear;
		} else {
			$year = 2000 + $decades*10 + $decyear;
		}

		switch($yearsem){
			case 2:
				$sem = "Fall";
				$year -= 1;		//This term belongs to the next academic school year
				break;
			case 3:
				$sem = "Winter";
				$year -= 1;		//This term belongs to the next academic school year
				break;
			case 4:
				$sem = "Spring";
				break;
			case 6:
				$sem = "Summer";
				break;
		}

		return $sem . ' ' . $year;
	}
}
?>
