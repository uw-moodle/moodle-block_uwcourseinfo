<?php
/******************************************************************************
* UW Madison Course Information Block - Instance Settings Form
*
* Instance configuration form for Course Information Block.
*
* Author: Nick Koeppen
******************************************************************************/
require_once($CFG->dirroot."/blocks/html/edit_form.php");

class block_uwcourseinfo_edit_form extends block_html_edit_form {
    const BLOCK_NAME = 'block_uwcourseinfo';
    const AUTO_MODE = 1;
    const CUSTOM_MODE = 2;

    protected $mode;

    protected function specific_definition($mform) {
    	global $OUTPUT;

    	// Do not render form if not applicable
    	$coursecontext = $this->block->context->get_course_context();
    	if (! ($coursecontext instanceof context_course)) {
    		return;
    	}

    	$siteconfig = get_config(self::BLOCK_NAME);

    	// or not allowed
    	if (!$siteconfig->allowinstanceconfig) {
    	    return;
    	}

    	$default = !empty($this->block->config->iscustom) ? self::CUSTOM_MODE : self::AUTO_MODE;
    	$this->mode = optional_param('mode', $default, PARAM_INT);

    	$iscustom = ($this->mode == self::CUSTOM_MODE);
    	if ($iscustom) {
    	    /* Expose HTML block base */
    	    parent::specific_definition($mform);
       	} else  {
    	    $switchlabel = get_string('switchtocustommode', self::BLOCK_NAME);

    	    if(isset($this->block->config->roles)){
    	        $siteconfig = $this->block->config;
    	        $siteconfig->roles = array_keys($siteconfig->roles);
    	    }else{
    	        $siteconfig->roles = explode(',', $siteconfig->roles);
    	    }
    	    /* Display automatic display settings */
    	    $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

    	    // Display roles
    	    $elements = array();
    	    $roles = get_roles_used_in_context($coursecontext);
    	    foreach($roles as $role){
    	    	$element = &$mform->createElement('checkbox', $role->id,''," ".$role->name);
    	    	$elements[$role->sortorder] = $element;
    	    }
    	    $mform->addElement('group','config_roles', get_string('roles',self::BLOCK_NAME), $elements, '<br>');
    	    foreach($siteconfig->roles as $roleid){
    	    	$mform->setDefault("config_roles[$roleid]", 1);
    	    }
    	    // Firstname
    	    $yesno = array('yes'=>get_string('yes'), 'no'=>get_string('no'));
    	    $mform->addElement('select', 'config_firstnames', get_string('firstnames', self::BLOCK_NAME), $yesno);
    	    $mform->setDefault('config_firstnames', $siteconfig->firstnames);
    	    // Name formatting
    	    $options = array('normal'=>get_string('normal_name',self::BLOCK_NAME),
    	    		'reverse'=>get_string('reverse_name',self::BLOCK_NAME));
    	    $mform->addElement('select', 'config_nameorder', get_string('nameorder', self::BLOCK_NAME), $options);
    	    $mform->setDefault('config_nameorder', $siteconfig->nameorder);
    	}

    	$mform->addElement('hidden', 'mode', $this->mode);

    	$mform->addElement('hidden', 'config_iscustom', $iscustom);
    }

    function display(){
        global $PAGE, $OUTPUT;

        $url = $PAGE->url;
        $url->param('sesskey', sesskey());

        if ($this->mode == self::CUSTOM_MODE) {
            $heading = get_string('custommode', self::BLOCK_NAME);
            $desc = get_string("mode_custom_descript", self::BLOCK_NAME);

            $label = get_string('switchtoautomode', self::BLOCK_NAME);
            $confirm = get_string('mode_toauto_notice', 'block_uwcourseinfo');
            $url->param('mode', self::AUTO_MODE);
        } else {
            $heading = get_string('automode', self::BLOCK_NAME);
            $desc = get_string("mode_auto_descript", self::BLOCK_NAME);

            $label = get_string('switchtocustommode', self::BLOCK_NAME);
            $confirm = get_string('mode_tocustom_notice', 'block_uwcourseinfo');
            $url->param('mode', self::CUSTOM_MODE);
        }

        echo $OUTPUT->box_start();
        echo $OUTPUT->heading($heading);
        echo html_writer::tag('p', $desc, array('class' => 'centerpara'));
        $button = new single_button($url, $label);
        $button->id = 'togglebutton';
        $button->add_action(new confirm_action($confirm, null, get_string('continue')));

        echo $OUTPUT->container($OUTPUT->container($OUTPUT->render($button), 'controls'), 'submitbtns mdl-align');
        echo $OUTPUT->box_end();

        parent::display();
    }

    function set_data($defaults) {
        $defaultmode = !empty($this->block->config->iscustom) ? self::CUSTOM_MODE : self::AUTO_MODE;
        $mode = optional_param('mode', $defaultmode, PARAM_INT);

        if ($mode == self::CUSTOM_MODE) {
            parent::set_data($defaults);
        } else {
            block_edit_form::set_data($defaults);
            //Trigger rebuild of default content
            $pageurl = $this->page->url;
            $pageurl->params(array('sesskey'=>sesskey(), 'uwcourseblockreset'=>true));
            $this->page->set_url($pageurl);
        }
    }

}