<?php
/******************************************************************************
* UW Madison Course Information Block - Global Settings
*
* Global (site-wide) configuration settings for Course Information block.
*
* Author: Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $blockname = 'block_uwcourseinfo';
    $configs = array();

    /*====== Settings ======*/
    $configs[] = new admin_setting_configcheckbox('allowinstanceconfig', get_string('allowinstanceconfig', $blockname), get_string('configallowinstanceconfig', $blockname), 1);

    //Role selection for appearance
    $defaults = array();
    if($wisc = get_config("enrol_wisc")){
        global $DB;
        if (!isset($wisc->teacherrole)) {
            // enrol_wisc isn't configured yet, so just use a default.
            $editingteacher = get_archetype_roles('editingteacher');
            $editingteacher = reset($editingteacher);
            $roles = array($editingteacher->id);
        } else {
            $roles = array_unique(array($wisc->teacherrole, $wisc->tarole, $wisc->otherrole));
        }
        foreach($roles as $role){
            $defaults[] = $DB->get_record('role', array('id'=>$role))->archetype;
        }
    }
    $configs[] = new admin_setting_pickroles('roles', get_string('roles',$blockname), get_string('configroles',$blockname), $defaults);

    //Formatting defaults
    $configs[] = new admin_setting_configcheckbox('firstnames', get_string('firstnames', $blockname), get_string('configfirstnames', $blockname), 0);

    $options = array('normal'=>get_string('normal_name',$blockname),
                 'reverse'=>get_string('reverse_name',$blockname));
    $configs[] = new admin_setting_configselect('nameorder', get_string('nameorder', $blockname), get_string('confignameorder', $blockname), 'normal', $options);

    foreach ($configs as $config) {
        $config->plugin = $blockname;
        $settings->add($config);
    }
}