<?php
/******************************************************************************
* UW Madison Course Information Block - Version Identifier
*
* Moodle code fragment to establish plugin version and cron cycle.
*
* Author: Nick Koeppen
******************************************************************************/
/**
 * Version details
 *
 * @package    block
 * @subpackage uwcourseinfo
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2012071601;
$plugin->cron      = 0;
$plugin->component = 'block_uwcourseinfo';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = 'Fall2012';
$plugin->dependencies = array(
    'enrol_wisc' => 2012021401,
);

?>
