****************************************************************************************************
UW-Madison Moodle plugins
- AUTHOR: Nick Koeppen (nkoeppen@wisc.edu)
- UPDATE: 21 Feb 2012
****************************************************************************************************
- TYPE: block
- NAME: uwcourseinfo (UW Course Information)
- REQUIRES:
 - blocks/html
 - enrol/wisc
****************************************************************************************************
*****  Description   *****
This block uses the WISC Enrollments (enrol/wisc) associations to display course information. The  block provides configurable default information.  Since this is an extension of the HTML block, a course manager or instructor can add, edit, or delete the content.

***** Default blocks *****
Moodle 2.x:

Default blocks in Moodle 2.x are hard coded in config.php by setting the defaultblocks_override
variable.  Refer to config-dist.php for more documentation.

- Format:
* The block names are in CSV (comma-separated values).
* The colon separates blocks on the left and right sides.

- Example:

$CFG->defaultblocks_override = 'participants,course_list:uwcourseinfo,news_items';