<?php

$string['pluginname'] = 'UW Course Information';
$string['blocktitle'] = 'UW Course';

//Global configuration strings
$string['allowinstanceconfig'] = "Allow Instance Configurations";
$string['configallowinstanceconfig'] = "Determine whether each block instance display can be configured. e.g. name display, additional users, roles";
$string['roles'] = "Display roles";
$string['configroles'] = "Select which roles to display in Roster Associations block.";

//Configuration instance strings
$string['automode'] = 'Automatic content';
$string['blocksettings'] = 'UW course display settings';
$string['configfirstnames'] = "Display full names for users instead of default first initial.  e.g. John Smith instead of J. Smith";
$string['confignameorder'] = "Order of display names for users.";
$string['currentmode'] = 'Current mode';
$string['custommode'] = 'Custom content';
$string['firstnames'] = "Show first names";
$string['mode_auto_descript'] = 'Automatic content mode displays content according to the settings provided below.';
$string['mode_custom_descript'] = 'Custom content mode allows for full HTML editing using the editor below';
$string['mode_tocustom_notice'] = 'Switching to custom content mode will prevent future updates to roster information.';
$string['mode_toauto_notice'] = 'Switching to automatic content mode will cause all customizations to be removed.';
$string['nameorder'] = "Name format";
$string['normal_name'] = "Normal - First Last";
$string['reverse_name'] = "Reverse - Last, First";
$string['rebuild'] = 'Rebuild';
$string['stylingpreview'] = 'Styling Preview';
$string['switchtoautomode'] = 'Switch to automatic content...';
$string['switchtocustommode'] = 'Switch to custom content...';

$string['uwcourseinfo:view'] = 'View UW Course Information block';


?>
