<?php
/******************************************************************************
* UW Madison Roster Associations Block
*
* Display roster to Moodle associations and course information.
*
* Author: Nick Koeppen
******************************************************************************/

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
	'block/uwcourseinfo:view' => array(
		'riskbitmask' => RISK_PERSONAL,
		'captype' => 'read',
		'contextlevel' => CONTEXT_BLOCK,
		'archetypes' => array(
			'guest'   => CAP_PROHIBIT,
            'user'    => CAP_ALLOW,
            'manager' => CAP_ALLOW
		)
	),
    'block/uwcourseinfo:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
                'editingteacher' => CAP_ALLOW,
                'manager' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:manageblocks'
    ),
);

?>